extends Node2D

var current = null
var drag_offset = Vector2()

var candidates = []

export var drag_group = "draggable"

func make_item(partname, player):
	var sprite = get_node_or_null(partname)
	if not sprite:
		return
	var texture = sprite.texture
	var image = texture.get_data()
	var region_rect = sprite.region_rect
	image = image.get_rect(region_rect)
	# The important part starts here
	var bitmap = BitMap.new()
	# create the bitmap from the image, you can set the alpha threshold here
	bitmap.create_from_image_alpha(image, 0.1)
	var rect = Rect2(Vector2(), image.get_size())
	# grow it if you need
	bitmap.grow_mask(2, rect)
	# this will convert all the opaque parts into polygons
	var polygons = bitmap.opaque_to_polygons(rect, 2)
	# create the CollisionPolygon2d
	var area = Area2D.new()
	for polygon in polygons:
		var collision = CollisionPolygon2D.new()
		# set its polygon to the first polygon you've got
		collision.polygon = polygon
		#collision.position = Vector2(10, 100)
		# and add it to the node
		area.add_child(collision)
	area.add_to_group(drag_group)
	add_child_below_node(sprite, area)
	area.position = sprite.position
	area.set_process(true)
	remove_child(sprite)
	area.add_child(sprite)
	area.set_script(preload("res://dollpart.gd"))
	area.name = sprite.name
	sprite.position = Vector2()
	area.position.x += 320
	area.connect('part_changed', player, 'set_part_visible')
	area.connect("mouse_entered",self,"mouse_entered",[area])
	area.connect("mouse_exited",self,"mouse_exited",[area])
	area.connect("input_event",self,"input_event",[area])

func _ready():
	visible = false

func _process(delta):
	if current is Node2D:
		current.global_position = current.get_global_mouse_position() - drag_offset
	if Input.is_action_just_pressed("ui_cancel"):
		visible = !visible

func mouse_entered(which):
	candidates.append(which)
	pass

func mouse_exited(which):
	candidates.erase(which)
	pass

func input_event(viewport: Node, event: InputEvent, shape_idx: int,which:Node2D):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.is_pressed():
				pick_up()
			else:
				drop_current()

func pick_up():
	candidates.sort_custom(self,"depth_sort")
	var last = candidates.back()
	if last:
		current = last
		drag_offset = current.get_global_mouse_position() - current.global_position
		if current.has_method("on_drag_start"):
			current.on_drag_start()

func drop_current(force_drop = false):
	var can_drop = true
	if current:
		if current.has_method("on_drop"):
			var on_drop_result = current.on_drop()
			can_drop = on_drop_result == null || on_drop_result
		if can_drop || force_drop:
			current = null

func depth_sort(a,b):
	return b.get_index()<a.get_index()