extends Area2D

signal part_changed(name, partvisible)

const PIN_DISTANCE = 32

var original_position = position

func from_pin_position():
	return position.distance_to(original_position)

func _process(delta):
	var doll = get_parent()
	var pin = doll.get_node(name+'-pin')
	if pin:
		var dist = from_pin_position()
		pin.visible = dist < PIN_DISTANCE

func on_drag_start():
	var doll = get_parent()
	var pin = doll.get_node(name+'-pin')
	if pin:
		pin.modulate.a = 0.5
	emit_signal('part_changed', name, false)

func on_drop():
	var doll = get_parent()
	var pin = doll.get_node(name+'-pin')
	if pin:
		var dist = from_pin_position()
		if dist < PIN_DISTANCE:
			position = original_position
			emit_signal('part_changed', name, true)
		pin.modulate.a = 1