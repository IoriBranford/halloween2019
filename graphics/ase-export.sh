#!/bin/bash
aseprite -b --list-tags --trim --sheet-pack --split-layers --shape-padding 1 --data $1.json --sheet $1.png $1.ase
