<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="env_ground" tilewidth="16" tileheight="16" tilecount="900" columns="30">
 <image source="env_ground.png" width="480" height="480"/>
 <tile id="360">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="16">
    <polygon points="0,0 16,-8 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="361">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8">
    <polygon points="0,0 16,0 16,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="382">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0">
    <polygon points="0,0 16,8 16,16 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="383">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8">
    <polygon points="0,0 16,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="384">
  <objectgroup draworder="index">
   <object id="1" x="16" y="8">
    <polygon points="0,0 -8,0 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="385">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8">
    <polygon points="0,0 8,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="388">
  <objectgroup draworder="index">
   <object id="1" x="8" y="16">
    <polygon points="0,0 8,-8 8,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="389">
  <objectgroup draworder="index">
   <object id="2" x="0" y="8">
    <polygon points="0,0 16,0 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="393">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="400">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="16">
    <polygon points="0,0 16,-8 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="401">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8">
    <polygon points="0,0 0,8 16,0 16,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="402">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0">
    <polygon points="0,0 16,8 16,16 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="403">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8">
    <polygon points="0,0 16,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="410">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="415">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0">
    <polygon points="0,0 8,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="416">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="417">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="418">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8">
    <polygon points="0,0 8,-8 8,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="426">
  <objectgroup draworder="index">
   <object id="1" x="16" y="8">
    <polygon points="0,0 -16,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="427">
  <objectgroup draworder="index">
   <object id="1" x="16" y="0">
    <polygon points="0,0 -16,8 -16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="428">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="429">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="431">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="432">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="434">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="435">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="436">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,8 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="437">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8">
    <polygon points="0,0 16,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="451">
  <objectgroup draworder="index">
   <object id="1" x="16" y="0">
    <polygon points="0,0 -16,0 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="452">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="453">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="454">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,8 16,16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="455">
  <objectgroup draworder="index">
   <object id="1" x="16" y="0">
    <polygon points="0,0 -8,8 0,16"/>
   </object>
   <object id="2" x="8" y="8"/>
  </objectgroup>
 </tile>
 <tile id="467">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="468">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 8,8 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="469">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,0 16,8 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="470">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="471">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="472">
  <objectgroup draworder="index">
   <object id="2" x="16" y="0">
    <polygon points="0,0 -16,8 -16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="478">
  <objectgroup draworder="index">
   <object id="1" x="0" y="16">
    <polygon points="0,0 16,-8 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="479">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8">
    <polygon points="0,0 16,0 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="488">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="489">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="490">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="491">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="492">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="493">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="494">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="495">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="504">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8">
    <polygon points="0,0 16,0 16,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="505">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="506">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8">
    <polygon points="0,0 16,-8 16,0 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="507">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="512">
  <objectgroup draworder="index">
   <object id="1" x="16" y="8">
    <polygon points="0,0 -16,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="513">
  <objectgroup draworder="index">
   <object id="1" x="16" y="0">
    <polygon points="0,0 -16,8 -16,16 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="514">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="529">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="530">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,8 16,16 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="531">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8">
    <polygon points="0,0 16,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="541">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0">
    <polygon points="0,0 0,8 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="548">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="549">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="550">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="551">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="552">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="553">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="554">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="555">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="562">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0">
    <polygon points="0,0 16,0 16,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="564">
  <objectgroup draworder="index">
   <object id="1" x="16" y="8">
    <polygon points="0,0 -16,0 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="565">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8">
    <polygon points="0,0 8,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="568">
  <objectgroup draworder="index">
   <object id="1" x="16" y="8">
    <polygon points="0,0 -8,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="569">
  <objectgroup draworder="index">
   <object id="1" x="8" y="8">
    <polygon points="0,0 -8,0 -8,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="573">
  <objectgroup draworder="index">
   <object id="1" x="16" y="0">
    <polygon points="0,0 -8,8 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="590">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 8,8 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="595">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0">
    <polygon points="0,0 8,8 8,16 -8,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="596">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="597">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="598">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8">
    <polygon points="0,0 8,-8 8,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="608">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="609">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="610">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="612">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="613">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="614">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="615">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="620">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="632">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="16">
    <polygon points="8,0 16,-8 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="638">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="645">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="651">
  <objectgroup draworder="index">
   <object id="1" x="8" y="16">
    <polygon points="0,0 -8,-8 -8,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="654">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8">
    <polygon points="0,0 16,8 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="655">
  <objectgroup draworder="index">
   <object id="1" x="16" y="16">
    <polygon points="0,0 -16,-8 -16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="664">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="665">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="666">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="667">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="668">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="675">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="676">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="677">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="678">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="679">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="681">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0">
    <polygon points="0,0 8,8 8,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="686">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="687">
  <objectgroup draworder="index">
   <object id="1" x="16" y="8">
    <polygon points="0,0 -16,-8 -16,0 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="688">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="689">
  <objectgroup draworder="index">
   <object id="1" x="16" y="8">
    <polygon points="0,0 -16,0 -16,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="751">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="752">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="781">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="782">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="786">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="16">
    <polygon points="0,0 16,-8 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="787">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8">
    <polygon points="0,0 16,-8 16,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="788">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0">
    <polygon points="0,0 0,16 16,8 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="789">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="790">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="791">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="792">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="793">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="794">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="795">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="796">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="797">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0">
    <polygon points="0,0 16,0 16,16 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="798">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0">
    <polygon points="0,0 16,8 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="799">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="8">
    <polygon points="0,0 16,8 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="815">
  <objectgroup draworder="index">
   <object id="2" type="one-way" x="0" y="0">
    <polygon points="0,0 16,8 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="830">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0">
    <polygon points="0,0 16,0 0,8"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
