extends KinematicBody2D

const GRAVITY = 360
const WALK_SPEED = 60
const RUN_SPEED = 120
const FLOOR_NORMAL = Vector2.UP
const WINGS_JUMP_VEL = -120
const WINGS_AIR_ACCEL_X = 120
const WINGS_MAX_FALL_SPEED = 60

var _velocity = Vector2()

var parts = [
	"wings",
	"tail"
]

var nonloop_animations = {
	'flap': true
}

func _ready():
	for partname in parts:
		set_part_visible(partname, false)
		$ui/doll.make_item(partname, self)
	$SpriteGroup/AnimationPlayer.connect("animation_finished", self, "animation_finished")
	for animname in nonloop_animations:
		var anim = $SpriteGroup/AnimationPlayer.get_animation(animname)
		anim.loop = false

func set_part_visible(partname, partvisible):
	var part = $SpriteGroup.get_node_or_null(partname)
	if part:
		part.visible = partvisible

func is_part_visible(partname):
	var part = $SpriteGroup.get_node_or_null(partname)
	return part and part.visible

func flap_wings():
	var animation = $SpriteGroup/AnimationPlayer.current_animation
	if animation != 'flap':
		_velocity.y = WINGS_JUMP_VEL
		return 'flap'
	return animation

func _physics_process(delta):
	var animation = $SpriteGroup/AnimationPlayer.current_animation
	var has_wings = $SpriteGroup/wings.visible
	var gravity = GRAVITY
	var snap = Vector2(0, 8)
	if has_wings:
		if Input.is_action_just_pressed("ui_accept"):
			animation = flap_wings()
			snap.y = 0
		gravity /= 4
	_velocity.y += gravity*delta
	_velocity = move_and_slide_with_snap(_velocity, snap, FLOOR_NORMAL, true, 4, PI/3)

	var input_action = Input.is_action_pressed('ui_accept')
	var input_right = Input.is_action_pressed("ui_right")
	var input_left = Input.is_action_pressed("ui_left")
	var input_x = 0
	if input_right:
		input_x += 1
	if input_left:
		input_x -= 1

	if is_on_floor():
		animation = 'stand'
		if input_x != 0:
			if input_action:
				animation = 'run'
			else:
				animation = 'walk'
			$SpriteGroup.scale.x = input_x

		var speed = RUN_SPEED if input_action else WALK_SPEED
		if is_on_floor():
			_velocity = get_floor_velocity()
		else:
			_velocity.x = get_floor_velocity().x

		_velocity.x += input_x*speed
	else:
		if has_wings:
			if animation != 'flap':
				animation = 'drop'
			_velocity.x += input_x*WINGS_AIR_ACCEL_X*delta
			_velocity.x = max(-RUN_SPEED, min(_velocity.x, RUN_SPEED))
			_velocity.y = min(_velocity.y, WINGS_MAX_FALL_SPEED)

			if input_x != 0:
				$SpriteGroup.scale.x = input_x
		else:
			animation = 'drop'

	$SpriteGroup/AnimationPlayer.play(animation)

func animation_finished(animname):
	if animname == 'flap':
		$SpriteGroup/AnimationPlayer.play("drop")
